<!DOCTYPE html>
<html lang = "en">

<div id="header">
<header class = "mainheader">
<IMG SRC="img/green_eagle.jpg" ALT="some text" >
<title> Eagle Financial Services home page </title>
    <meta charset = "UTF-8">
    <meta name ="viewport" content = "width = device - width, initial - scale = 1.0">


                <nav>
                     <ul>
                        <li><a href = '/eagle'>EAGLE FINANCIAL SERVICES</a>
                        <li><a href = '#'>PORTFOLIO PLANNER OPTIONS</a>
                            <ul class = "submenu" >
                                  <li><a href="/eagle/customers">Customer and Review Profile</a></li>
                                  <li><a href="/eagle/stocks">Update Stock Portfolio options</a></li>
                                  <li><a href="/eagle/investments">Update Non Stock investments</a></li>
                                  <li><a href="/eagle/projects">View and Update Project Details</a></li>
                                  <li><a href="/eagle/users">View and Update Access Details</a></li>

                            </ul>
                        </li>
                     </ul></li>
                     </ul>
                </nav>
                </header>
         </div>

    <div id="mainContent">

    <div class = "mainContent">
       <body background="img/blue.jpg">
            <div class = "content">
            <article class = "topcontent"
                <header>

                    <h2> Testimonial </h2>

                    <content>

                    <p><b>Eagle Financial Services</b> provided the short term financing we needed to make it over the rough terrain our
                    business hit earlier in the year. Their process is straight forward and efficient and funding was
                    much quicker than any other financing option. <br>Thanks for your help in getting us back to calmer waters.</p>
                    <content>
                    <footer> Anson, Dsouza - Omaha Nebraska </footer>
            </article>


         </div>
     </div>



        <aside class = "bottom-sidebar">
                        <h2> Why Eagle Financial Services?</h2>
                                <p>
                                      <b>Convinience</b> - 24x7 access to a wide range of transactions<br>
                                      <b>Investment</b> - Grow your money with smart easy ways to invest online
                                </p>
        </aside>
    </div>


   <div id="footer"></div>
    		<footer class = "mainFooter">
                   <p ><b>Copyright &copy; EAGLE FINANCIAL SERVICES LIMITED. ALL RIGHTS RESERVED</b </p>
            </footer>
   </div>

</body>
</html>



